admin_passarr = []
admin_IDarr = []
def set_admin_ID() :
    admin_ID = input("enter an ID (ID doesn't exceed 12 character): ")
    if len(admin_ID)<=12:
        if admin_ID in admin_IDarr:
            print("ID already taken please enter another one")
            set_admin_ID()
        else:
            print("confirmed ID")
            admin_IDarr.append(admin_ID)
    else:
        print("Your ID must not exceed 12 character\nplease renter another one ")
        set_admin_ID()

def set_admin_pass() :
    admin_pass = input("enter a password (your password must be from 6 to 20 character and there must be an uppercase and lowercase letter and a number and a special character($#@%) : ")
    
    SpecialSym =['$', '@', '#', '%']
    val = True
      
    if len(admin_pass) < 6:
        print('length should be at least 6')
        val = False
          
    if len(admin_pass) > 20:
        print('length should be not be greater than 20')
        val = False
          
    if not any(char.isdigit() for char in admin_pass):
        print('Password should have at least one numeral')
        val = False
          
    if not any(char.isupper() for char in admin_pass):
        print('Password should have at least one uppercase letter')
        val = False
          
    if not any(char.islower() for char in admin_pass):
        print('Password should have at least one lowercase letter')
        val = False
          
    if not any(char in SpecialSym for char in admin_pass):
        print('Password should have at least one of the symbols $@#')
        val = False
        
    if val == True:
        confpass = input("please renter the pass to confirm the password")
        if(admin_pass==confpass):
            admin_passarr.append(admin_pass)
            print("password confirmed")
        else:
            print("the password you renetered doesn't match your password please renter a password and confirm it")
            set_admin_pass()
    else:
        set_admin_pass()



def add_admin() :
    set_admin_ID()
    set_admin_pass()
    print('admin successfully added')
    
add_admin()