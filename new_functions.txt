import re
passarr = []
def createpass() :
    cpass = input("enter a password (your password must be from 6 to 20 character and there must be an uppercase and lowercase letter and a number and a special character($#@%) : ")
    
    SpecialSym =['$', '@', '#', '%']
    val = True
      
    if len(cpass) < 6:
        print('length should be at least 6')
        val = False
          
    if len(cpass) > 20:
        print('length should be not be greater than 8')
        val = False
          
    if not any(char.isdigit() for char in cpass):
        print('Password should have at least one numeral')
        val = False
          
    if not any(char.isupper() for char in cpass):
        print('Password should have at least one uppercase letter')
        val = False
          
    if not any(char.islower() for char in cpass):
        print('Password should have at least one lowercase letter')
        val = False
          
    if not any(char in SpecialSym for char in cpass):
        print('Password should have at least one of the symbols $@#')
        val = False
        
    if val == True:
        conpass = input("please renter the pass to confirm the password")
        if(cpass==conpass):
            passarr.append(cpass)
            print("password confirmed")
        else:
            print("the password you renetered doesn't match your password please renter a password and confirm it")
            createpass()
    else:
        createpass()
        
        
createpass()












import re
IDarr = []
def createID() :
    ID = input("enter an ID (ID doesn't exceed 12 character): ")
    if len(ID)<=12:
        if ID in IDarr:
            print("ID already taken please enter another one")
            createID()
        else:
            print("confirmed ID")
            IDarr.append(ID)
    else:
        print("Your ID must not exceed 12 character\nplease renter another one ")
        createID()
        

createID()